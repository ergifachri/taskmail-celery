# tests/front_end_tests.py

import unittest
import urllib2

from flask_testing import LiveServerTestCase
from selenium import webdriver

from config import *
from flask import abort, url_for

from app import create_app, db,Index
from models.Taskmail import Taskmail


# Set test variables for test taskmail user
test_user_email_recipient = "ergifachri@gmail.com"
test_user_email_subject = "Ping !"
test_user_email_content = "Hello"
test_user_email_timestamp = "2021-01-22 00:31:00"



class TestBase(LiveServerTestCase):

    def create_app(self):
        config_name = 'testing'
        app = create_app(config_name)
        app.config.update(
            # Specify the test database
            SQLALCHEMY_DATABASE_URI='mysql://dt_admin:dt2016@localhost/dreamteam_test',
            # Change the port that the liveserver listens on
            LIVESERVER_PORT=8943
        )
        return app

    def setUp(self):
        """Setup the test driver and create test users"""
        self.driver = webdriver.Chrome()
        self.driver.get(self.get_server_url())

        db.session.commit()
        db.drop_all()
        db.create_all()

        # create test admin user
        self.taskmail = Taskmail(email_recipient=test_user_email_recipient,
                              email_subject=test_user_email_subject,
                              email_content=test_user_email_content,
                              email_timestamp=test_user_email_timestamp)

        # save users to database
        db.session.add(self.taskmail)
        db.session.commit()

    def tearDown(self):
        self.driver.quit()

    def test_server_is_up_and_running(self):
        response = urllib2.urlopen(self.get_server_url())
        self.assertEqual(response.code, 200)


class TestSendEmailForm(TestBase):

    def test_send_email(self):
        """
        Test that a user can create send and email using the registration form
        if all fields are filled out correctly
        """

        # Click register menu link
        self.driver.find_element_by_id("sendmail_link").click()
        time.sleep(1)

        # Fill in registration form
        self.driver.find_element_by_id("timestamp").send_keys(test_employee2_email)
        self.driver.find_element_by_id("email_recipient").send_keys(
            test_employee2_username)
        self.driver.find_element_by_id("email_subject").send_keys(
            test_employee2_first_name)
        self.driver.find_element_by_id("email_content").send_keys(
            test_employee2_last_name)
        self.driver.find_element_by_id("submit").click()
        time.sleep(1)


        # Assert that there are now 2 taskmails in the database
        self.assertEqual(Taskmail.query.count(), 2)

if __name__ == '__main__':
    unittest.main()