# tests/test_back_end.py

import unittest
from config import *
from flask import abort, url_for
from flask_testing import TestCase

from app import create_app, db,Index
from models.Taskmail import Taskmail


class TestBase(TestCase):

    def create_app(self):

        # pass in test configurations
        config_name = 'testing'
        app = Flask(__name__)
        app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql+pymysql://' + config["mysql"]["user"] + ':' + config["mysql"]["pass"] + '@' + config["mysql"]["ip"] + '/' + config["mysql"]["schema"]
        app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
        db.init_app(app)
        return app

    def setUp(self):
        """
        Will be called before every test
        """

        db.session.commit()
        db.drop_all()
        db.create_all()

        # create test admin user
        taskmail = Taskmail(email_subject="ergifachri@gmail.com", email_content="Ping!", timestamp="2021-01-30 00:18:00",code_sta="QUEUE")

        

        # save users to database
        db.session.add(taskmail)
        db.session.commit()

    def tearDown(self):
        """
        Will be called after every test
        """

        db.session.remove()
        db.drop_all()


class TestModels(TestBase):

    def test_taskmail_model(self):
        """
        Test number of records in Taskmail table
        """
        self.assertEqual(Taskmail.query.count(), 1)

    


class TestViews(TestBase):

    def test_homepage_view(self):
        """
        Test that homepage is accessible without login
        """
        response = self.client.get(url_for('Index'))
        self.assertEqual(response.status_code, 200)

    




if __name__ == '__main__':
    unittest.main()