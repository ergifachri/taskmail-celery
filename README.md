# This is task scheduler using Celery and Redis to send email

Setup your flask project


# Install Celery
    pip install celery

# Install SQLAlchemy
    pip install SQLAlchemy

# Install flask_mail
    pip install Flask-Mail

Setup your MySQL Database and Redis

# Redis
    
- Download Redis docker setup in this link
    [(https://github.com/ProgrammerZamanNow/belajar-redis.git)]
- Go to directory Redis
- Run this script : docker-compose -f .\docker-compose.yaml up -d

# Run your app
- [ Start up Redis ]
- [ Start up your Flask Apps]  
- [ Start up your celery project using this command : celery -A 'your celery worker' worker --loglevel=INFO ] 
- [Start up celery flower using this command : celery flower -A app.client --address=127.0.0.1 --port=5555]

# UnitTesting
    Unit Test has two files which is :
        1. test_back_end.py
        2. test_front_end.py

    Install nose2, selenium and dont forget to set up chrome driver
        pip install nose2
        pip install selenium
    After Install you can run in command prompt
        nose2




