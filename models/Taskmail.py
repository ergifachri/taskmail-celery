from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()

class Taskmail(db.Model):
    id = db.Column(db.String(100),primary_key=True)
    email_recipient = db.Column(db.String(100))
    email_subject = db.Column(db.String(100))
    email_content = db.Column(db.String(500))
    code_sta = db.Column(db.String(100))
    timestamp = db.Column(db.DateTime(timezone=True))

    def __init__(self,task_id,email_recipient,email_subject,email_content,code_sta,timestamp):
        self.id = task_id
        self.email_recipient=email_recipient
        self.email_subject = email_subject
        self.email_content = email_content
        self.code_sta = code_sta
        self.timestamp = timestamp