from flask import Flask,render_template,request,url_for,redirect,flash
from config import *
from models.Taskmail import db
from models.Taskmail import Taskmail
from flask_mail import Mail,Message
from celery import result,Celery
import time,pytz,datetime
import base64

app = Flask(__name__)
app.config['MAIL_SERVER'] = config["mail"]["server"]
app.config['MAIL_PORT'] = config["mail"]["port"]
app.config['MAIL_USE_SSL'] = config["mail"]["ssl"]
app.config['MAIL_USERNAME'] = config["mail"]["mail_username"]
app.config['MAIL_PASSWORD'] = config["mail"]["mail_password"]

app.secret_key = "Secret Key"

#setup celery client
client = Celery(app.name, broker='redis://'+ config["redis"]["ip"] +':'+ config["redis"]["port"] +'/0'
    , backend='redis://'+ config["redis"]["ip"] +':'+ config["redis"]["port"] +'/0')

app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql+pymysql://' + config["mysql"]["user"] + ':' + config["mysql"]["pass"] + '@' + config["mysql"]["ip"] + '/' + config["mysql"]["schema"]
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

mail = Mail(app)
db.init_app(app)

    


@client.task(bind=True)
def send_mail(self,data):
    """ Function to send emails in the background.
    """
    with app.app_context():
        try:
            taskid = self.request.id
            msg = Message(data['email_subject'],
                        sender="admin.ping",
                        recipients=[data['email_recipient']])
            msg.body = data['email_content']        
            mail.send(msg)

            #If Finish and Update code_sta
            my_data = Taskmail.query.get(taskid)
    
            my_data.code_sta = 'SENT'
            db.session.commit()
        
        except e:
            print("got exception")
            print(e)
   

#This is the index route where we are going to
#query on all our TaskMail data
@app.route('/')
def Index():
    all_data = Taskmail.query.all()
    return render_template("index.html", taskmails = all_data)
 
 
 

 
 
#This route is for deleting our Taskmail
@app.route('/delete/<id>/', methods = ['GET', 'POST'])
def delete(id):
    #revoke taskmail
    celery_task_result = result.AsyncResult(id,app=client)
    celery_task_result.revoke()
    my_data = Taskmail.query.get(id)
    db.session.delete(my_data)
    db.session.commit()
    flash("Taskmail Deleted Successfully")
 
    return redirect(url_for('Index'))
 



if __name__ =="__main__":
    app.run(debug=True)


from taskmails.taskmails import taskmails_bp
app.register_blueprint(taskmails_bp,url_prefix='/taskmails')