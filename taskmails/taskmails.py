from flask import Blueprint,render_template,request,redirect,url_for
from models.Taskmail import db
from models.Taskmail import Taskmail
from celery import result,Celery
from app import send_mail,Index,db
import time,pytz,datetime
import base64

taskmails_bp = Blueprint('taskmail_bp',__name__)

#this route is for inserting data to mysql database via html forms
@taskmails_bp.route('/save_email', methods = ['POST'])
def save_email():
 
    if request.method == 'POST':
        data = {}
        task_id = 0
        email_recipient = request.form['email_recipient']
        email_subject = request.form['email_subject']
        email_content = request.form['email_content']
        timestamp = request.form['timestamp']
        code_sta = 'QUEUED'

        #Converting string to date
        date_time_obj = datetime.datetime.strptime(timestamp,'%Y/%m/%d %H:%M').strftime('%Y-%m-%d %H:%M')
        local_datetime_converted = time.strftime("%Y-%m-%d %H:%M:%S", 
              time.gmtime(time.mktime(time.strptime(date_time_obj, 
                                                    "%Y-%m-%d %H:%M"))))

        #Email task queue
        data['email_recipient']=email_recipient
        data['email_subject'] = email_subject
        data['email_content'] = email_content
        task_id=send_mail.apply_async(args = [data],eta = local_datetime_converted).id
        
 
        my_data = Taskmail(task_id,base64.b64encode(email_recipient.encode("UTF-8")),email_subject, email_content,code_sta, date_time_obj)
        
        db.session.add(my_data)
        db.session.commit()
        return redirect(url_for('Index'))